/* Задания на урок:

1) Реализовать функционал, что после заполнения формы и нажатия кнопки "Подтвердить" - 
новый фильм добавляется в список. Страница не должна перезагружаться.
Новый фильм должен добавляться в movieDB.movies.
Для получения доступа к значению input - обращаемся к нему как input.value;
P.S. Здесь есть несколько вариантов решения задачи, принимается любой, но рабочий.

2) Если название фильма больше, чем 21 символ - обрезать его и добавить три точки

3) При клике на мусорную корзину - элемент будет удаляться из списка (сложно)

4) Если в форме стоит галочка "Сделать любимым" - в консоль вывести сообщение: 
"Добавляем любимый фильм"

5) Фильмы должны быть отсортированы по алфавиту */

const movieDB = {
    movies: [
        "Логан",
        "Лига справедливости",
        "Ла-ла лэнд",
        "Одержимость",
        "Скотт Пилигрим против..."
    ]
};
//----------------------------------------------//
//Предыдущий урок
const blocksRem = document.querySelectorAll('.promo__adv img');

blocksRem.forEach(item => {
    item.remove();
});

const poster = document.querySelector('.promo__bg'),
    genre = poster.querySelector('.promo__genre');

genre.textContent = 'драма';
poster.style.backgroundImage = 'url("img/bg.jpg")'

const movieList = document.querySelector(".promo__interactive-list");
//------------------------------------------------//

//Выполнила задание, но не доработала изменение нумерациии при удалении фильмов

function updateMovie() {

    movieList.innerHTML = "";
    movieDB.movies.sort();
    movieDB.movies.forEach((item, i) => {
        movieList.innerHTML = movieList.innerHTML + `
            <li class="promo__interactive-item">${i + 1}. ${item}
                <div class="delete"></div>
            </li>`;
    });
    deleteButton();
}

function deleteButton(){
    removeBtn=document.querySelectorAll(".delete");//получили псведо-массив
        removeBtn.forEach((btn) => {
            btn.addEventListener("click", (event) =>
                deleteElement(event));               
        });
};

function deleteElement(event){
    event.currentTarget.parentElement.remove();
    movieDB.movies.splice(id, 1);
    updateMovie();
}


const btn = document.querySelector(".add button")

btn.addEventListener("click", function (event) {//назначаем обработчик

    event.preventDefault();
    let newMovie = document.querySelector(".adding__input").value;

    if (newMovie.length > 21) {
        newMovie = newMovie.slice(0, 21) + "...";
    };

    movieDB.movies.push(newMovie);
    updateMovie();

    let favoriteMovie = document.querySelector(".add input[type='checkbox']");
    if (favoriteMovie.checked) {
        console.log("Добавляем любимый фильм");
    };
});

updateMovie();